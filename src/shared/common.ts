interface Domain {
  development: string,
  production: string,
}

interface DomainResult {
  API_DOMAIN: string,
}

function getEnvDomains (apiDomains: Domain): DomainResult {
  let API_DOMAIN;

  if (process.env.NODE_ENV === 'development') {
    API_DOMAIN = apiDomains.development;
  }
  else {
    API_DOMAIN = apiDomains.production;
  }

  return { API_DOMAIN };
}

export {
  getEnvDomains,
};
