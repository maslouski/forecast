import { AxiosInstance } from 'axios';

export const apiCall = async (request: Promise<AxiosInstance>) => {
  try {
    return await request;
  } catch (error) {
    console.log(request);
    console.log(error);
    return error.response;
  }
};
