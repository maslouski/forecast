import { apiCall } from '../shared';
import api from '../consts/api';

const apiWeather = (city: string) => {
  const url = api.openWeatherMapCalls && api.openWeatherMapCalls.apiWeather;
  if (url) {
    return apiCall(api.get(url, {
      params: {
        q: city,
      },
    }));
  }
};

export default {
  apiWeather,
};
