import openweathermap from './openweathermap';

const Api = {
  ...openweathermap,
};

export default Api;
