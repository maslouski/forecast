import { configureStore, ThunkAction, Action } from '@reduxjs/toolkit';
import appReducer from './slices/app.slice';
import cityReducer from './slices/city.slice';

export const store = configureStore({
  reducer: {
    app: appReducer,
    city: cityReducer,
  },
});

/*eslint-disable */
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;
/*eslint-enable */
