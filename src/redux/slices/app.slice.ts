import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '../store';
import { DEFAULT_LOCALE, THEME_LIGHT } from '../../consts';

interface AppState {
  locale: string,
  theme: string,
}

const initialState: AppState = {
  locale: DEFAULT_LOCALE,
  theme: THEME_LIGHT,
};

export const appReducer = createSlice({
  name: 'app',
  initialState,
  reducers: {
    setLocale: (state, action: PayloadAction<string>) => {
      state.locale = action.payload;
    },
    setTheme: (state, action: PayloadAction<string>) => {
      state.theme = action.payload;
    },
  },
});

// actions
export const { setLocale, setTheme } = appReducer.actions;

// selectors
export const selectLocale = (state: RootState) => state.app.locale;
export const selectTheme = (state: RootState) => state.app.theme;

// reducer
export default appReducer.reducer;
