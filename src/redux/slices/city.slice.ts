import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '../store';
import { LS_CITIES, MAX_STORAGE_CITIES_AMOUNT } from '../../consts';

const initialState = {
  coord: {
    lat: 53.9,
    lon: 27.57,
  },
  weather: [{
    main: null,
    description: null,
  }],
  wind: {
    speed: null,
  },
  main: {
    pressure: null,
    temp: null,
  },
  name: '',
  cod: '',
  message: '',
  storage: new Array,
};

export const cityReducer = createSlice({
  name: 'city',
  initialState,
  reducers: {
    setCity: (state, action: PayloadAction<Object>) => {
      return {
        ...state,
        ...action.payload,
      };
    },
    setStoredCities: (state, action) => {
      state.storage = action.payload;
    },
    resetCode: (state) => {
      state.cod = '';
    },
    setStoredCity: (state, action: PayloadAction<string>) => {
      let res = state.storage;
      res.push(action.payload);
      res = res.filter((item, idx, arr) => arr.indexOf(item) === idx);
      if (res.length > MAX_STORAGE_CITIES_AMOUNT) {
        res.shift();
      }
      state.storage = res;
      localStorage.setItem(LS_CITIES, JSON.stringify(res));
    },
  },
});

// actions
export const { setCity, setStoredCity, setStoredCities, resetCode } = cityReducer.actions;

// selectors
export const selectCoords = (state: RootState) => {
  return {
    lat: state.city.coord.lat,
    lng: state.city.coord.lon,
  };
};

export const selectStorageCities = (state: RootState) => state.city.storage;
export const selectCode = (state: RootState) => state.city.cod;

export const selectWeather = (state: RootState) => {
  const weather = state.city.weather[0];
  const wind = state.city.wind;
  const main = state.city.main;
  return {
    code: parseInt(state.city.cod),
    message: state.city.message,
    main: weather.main,
    description: weather.description,
    windSpeed: wind.speed,
    pressure: main.pressure,
    name: state.city.name,
    temperature: main.temp,
  };
};

// reducer
export default cityReducer.reducer;
