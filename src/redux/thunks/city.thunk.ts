import { AppThunk } from '../store';
import { setCity, setStoredCity } from '../slices/city.slice';
import Api from '../../api';

export const setCityAsync = (city: string): AppThunk => async dispatch => {
  const response = await Api.apiWeather(city);
  if (response && response.data) {
    dispatch(setCity(response.data));
    if (response.data.name) {
      dispatch(setStoredCity(response.data.name));
    }
  }
  else {
    dispatch(setCity({
      cod: 500,
      message: 'Oops. Looks like connection error'
    }));
  }
};
