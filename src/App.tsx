import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import './App.css';
import { ThemeProvider } from 'styled-components';
import {
  SiteLogo,
  Copyright,
  SearchForm,
  MapContainer,
  WeatherInfo,
} from './components';
import LanguageSwitcher from './components/language-switcher.component';
import ThemeSwitcher from './components/theme-switcher.component';
import { Container, Row, Main, Section, Header, Footer } from './styled';
import { LS_LOCALE, LS_CITIES, LS_THEME } from './consts';
import { setLocale, setTheme, selectTheme } from './redux/slices/app.slice';
import { setStoredCities } from './redux/slices/city.slice';
import { Trans, useTranslation } from 'react-i18next';
import { ToastContainer, Slide } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

function App () {

  const dispatch = useDispatch();
  const theme = useSelector(selectTheme);
  const { i18n } = useTranslation();

  useEffect(() => {
    const locale = localStorage.getItem(LS_LOCALE);
    const cities = localStorage.getItem(LS_CITIES);
    const theme = localStorage.getItem(LS_THEME);
    if (locale) {
      dispatch(setLocale(locale));
      i18n.changeLanguage(locale);
    }
    if (cities) {
      dispatch(setStoredCities(JSON.parse(cities)));
    }
    if (theme) {
      dispatch(setTheme(theme));
    }
  }, []);

  return (
    <ThemeProvider theme={{ mode: theme }}>
      <Header>
        <Container>
          <Row>
            <ThemeSwitcher/>
            <SiteLogo/>
            <LanguageSwitcher/>
          </Row>
        </Container>
      </Header>
      <Main>
        <Section>
          <Container>
            <SearchForm/>
            <WeatherInfo/>
          </Container>
        </Section>
      </Main>
      <Footer>
        <MapContainer/>
        <Container>
          <Row>
            <Copyright>
              Copyright © {new Date().getFullYear()} <Trans>Viktar Maslouski</Trans>.
            </Copyright>
          </Row>
        </Container>
      </Footer>
      <ToastContainer transition={Slide}/>
    </ThemeProvider>
  );
}

export default App;
