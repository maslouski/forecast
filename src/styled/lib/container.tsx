import styled, { css } from 'styled-components';
import theme from 'styled-theming';

const themeStyles = theme('mode', {
  light: css`
    background-color: #fff;
    color: #121212;
  `,
  dark: css`
    background-color: #121212;
    color: #fff;
  `,
});

const themeStylesHeader = theme('mode', {
  light: css`
    box-shadow: 0px 0px 4px 2px rgba(0,0,0,0.2);
  `,
  dark: css`
    background-color: dimgrey;
    box-shadow: 0px 0px 4px 2px rgba(255,255,255,0.5);
  `,
});

export const Main = styled.main`
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-shrink: 0;
  ${themeStyles}
`;

export const Container = styled.div`
  max-width: 1170px;
  padding-left: 15px;
  padding-right: 15px;
  margin: 0 auto;
  flex-grow: 1;
`;

export const Section = styled.section`
  padding-top: 3rem;
  padding-bottom: 3rem;
  width: 100%;
`;

export const Header = styled.header`
  ${themeStylesHeader}
  z-index: 10;
`;

export const Footer = styled.footer`
  ${themeStyles}
  box-shadow: 0px 0px 4px 2px rgba(0,0,0,0.2);
  z-index: 10;
`;

export const Row = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding-top: 1rem;
  padding-bottom: 1rem;
`;
