import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import styled from 'styled-components';
import { selectLocale, setLocale } from '../redux/slices/app.slice';
import { LS_LOCALE } from '../consts';
import { useTranslation } from 'react-i18next';

const Switcher = styled.ul`
  margin: 0;
  padding: 0;
  list-style: none;
  display: flex;
`;

interface ButtonProps {
  active?: boolean;
}

const Button = styled.button`
  text-decoration: ${(props: ButtonProps) => props.active ? 'underline' : 'none'};
  &:hover {
    text-decoration: underline;
    cursor: ${(props: ButtonProps) => props.active ? 'default' : 'pointer'};
  }
  font-size: 1.4rem;
  line-height: 1.3em;
  color: ${(props: ButtonProps) => props.active ? '#f17649' : '#000'};
`;

const languages = [
  { title: 'En', value: 'en' },
  { title: 'Ru', value: 'ru' },
];

const LanguageSwitcher = () => {

  const dispatch = useDispatch();
  const { i18n } = useTranslation();
  const locale = useSelector(selectLocale);

  const handleLocaleChange = (lang: string) => {
    dispatch(setLocale(lang));
    i18n.changeLanguage(lang);
    localStorage.setItem(LS_LOCALE, lang);
  };

  return (
    <Switcher>
      {languages.map(lang => (
        <li
          key={`locale-${lang.value}`}
          style={{marginLeft: '1rem'}}>
          <Button
            active={locale === lang.value}
            value={lang.value}
            onClick={() => handleLocaleChange(lang.value)}
          >
            {lang.title}
          </Button>
        </li>
      ))}
    </Switcher>
  );
};

export default React.memo(LanguageSwitcher);
