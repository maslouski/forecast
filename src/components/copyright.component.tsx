import styled from 'styled-components';

export const Copyright = styled.div`
  font-size: 1.4rem;
  line-height: 1.3em;
  padding: 1rem;
  text-align: center;
  width: 100%;
`;
