import React from 'react';
import { useSelector } from 'react-redux';
import { compose, withProps } from 'recompose';
import { GoogleMap, withGoogleMap, withScriptjs, Marker } from 'react-google-maps';
import { selectCoords } from '../redux/slices/city.slice';
import { GOOGLE_MAP_API } from '../consts';

const Map = () => {

  const coords = useSelector(selectCoords);

  return (
    <GoogleMap
      key={new Date().getTime()}
      defaultZoom={8}
      defaultCenter={coords}
    >
      <Marker position={coords} />
    </GoogleMap>
  );
};

export const MapContainer = compose(
  withProps({
    googleMapURL: GOOGLE_MAP_API,
    loadingElement: <div style={{ width: '100%', height: '100%' }} />,
    containerElement: <div style={{ width: '100%', height: '30rem'}} />,
    mapElement: <div style={{ width: '100%', height: '100%' }} />,
  }),
  withScriptjs,
  withGoogleMap
)(Map);
