import React from 'react';
import styled from 'styled-components';
import { useDispatch, useSelector } from 'react-redux';
import { LS_THEME, THEME_LIGHT, THEME_DARK } from '../consts';
import { setTheme, selectTheme } from '../redux/slices/app.slice';

const width = '8rem';

const Switcher = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;

const Switcher__Wrapper = styled.span`
  position: relative;
  width: ${width};
  height: 3rem;
  border-radius: .5rem;
`;

const Switcher__Input = styled.input`
  transition: .25s -.1s;
  appearance: none;
  margin: 0;
  position: relative;
  width: inherit;
  height: inherit;
  border-radius: inherit;
  background-color: #222;
  outline: none;

  &:before,
  &:after {
    z-index: 2;
    position: absolute;
    top: 50%;
    transform: translateY(-50%);
    color: #fff;
    font-size: 1rem;
    line-height: 1.3em;
    font-weight: 700;
  }

  &:before {
    content: 'Light';
    left: 1rem;
  }

  &:after {
    content: 'Dark';
    right: 1rem;
  }

  &:checked {
    background-color: #fff;

    &:before {
      color: #fff;
      transition: color .5s .2s;
    }

    &:after {
      color: #222;
      transition: color .5s;
    }

    & + label {
      left: .2rem;
      right: calc(${width}/2);
      background-color: #222;
      transition: left .5s, right .4s .2s;
    }
  }
  &:not(:checked) {
    background-color: #222;
    transition: background .5s -.1s;

    &:before {
      color: #fff;
      transition: color .5s;
    }

    &:after {
      color: #222;
      transition: color .5s .2s;
    }

    & + label {
      left: calc(${width}/2);
      right: .2rem;
      background-color: #fff;
      transition: left .4s .2s, right .5s, background .35s -.1s;
    }
  }
`;

const Switcher__Label = styled.label`
  z-index: 1;
  position: absolute;
  top: .2rem;
  bottom: .2rem;
  border-radius: inherit;
`;

const ThemeSwitcher = () => {

  const dispatch = useDispatch();
  const theme = useSelector(selectTheme);

  const handleThemeChange = () => {
    const current = theme === THEME_LIGHT
      ? THEME_DARK
      : THEME_LIGHT;

    dispatch(setTheme(current));
    localStorage.setItem(LS_THEME, current);
  };

  return (
    <Switcher>
      <Switcher__Wrapper>
        <Switcher__Input
          id="switchThemeLight"
          type="checkbox"
          checked={theme === THEME_LIGHT}
          onChange={handleThemeChange}
        />
        <Switcher__Label htmlFor="switchThemeLight"/>
      </Switcher__Wrapper>
    </Switcher>
  );
};

export default React.memo(ThemeSwitcher);
