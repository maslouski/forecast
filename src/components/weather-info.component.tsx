import React, { useEffect } from 'react';
import styled from 'styled-components';
import { useSelector } from 'react-redux';
import { selectWeather } from '../redux/slices/city.slice';
import { Trans } from 'react-i18next';
import { toast } from 'react-toastify';
import { useTranslation } from 'react-i18next';

const WeatherInfo__Container = styled.div`
  padding-top: 2rem;
  text-align: center;
`;

const WeatherInfo__Title = styled.div`
  font-size: 2rem;
  line-height: 1.3em;
  margin-bottom: 2rem;
`;

const WeatherInfo__Table = styled.div`
  display: flex;
  justify-content: center;
  aligh-items: flex-start;
  @media (max-width: 767px) {
    flex-wrap: wrap;
  }
`;

const WeatherInfo__TableColumn = styled.div`
  padding: 1rem 2rem;
  text-align: center;
  &:not(:last-child) {
    border-right: 1px solid #ccc;
  }
  @media (max-width: 767px) {
    &:not(:last-child) {
      border-right: none;
      border-bottom: 1px solid #ccc;
      width: 100%;
      padding: 2rem 2rem;
    }
  }
`;

const WeatherInfo__TableColumnTitle = styled.h4`
  font-size: 1.6rem;
  line-height: 1.3em;
  margin-bottom: 1rem;
`;

const WeatherInfo__TableColumnDesc = styled.div`
  font-size: 1.4rem;
  line-height: 1.3em;
`;

export const WeatherInfo = () => {

  const weather = useSelector(selectWeather);
  const { t } = useTranslation();

  useEffect(() => {
    if (weather.code && weather.code !== 200) {
      toast.error(t(weather.message));
    }
  }, [weather.code]);

  if (!weather.name) {
    return null;
  }

  return (
    <WeatherInfo__Container>
      <WeatherInfo__Title>
        <Trans>Weather info in the city</Trans>:
        <strong> {weather.name}</strong>
      </WeatherInfo__Title>
      <WeatherInfo__Table>
        {weather.description &&
          <WeatherInfo__TableColumn>
            <WeatherInfo__TableColumnTitle>
              <strong><Trans>Overal</Trans></strong>
            </WeatherInfo__TableColumnTitle>
            <WeatherInfo__TableColumnDesc>{weather.description}</WeatherInfo__TableColumnDesc>
          </WeatherInfo__TableColumn>
        }
        {weather.pressure &&
          <WeatherInfo__TableColumn>
            <WeatherInfo__TableColumnTitle>
              <strong><Trans>Pressure</Trans>, mmHg</strong>
            </WeatherInfo__TableColumnTitle>
            <WeatherInfo__TableColumnDesc>{weather.pressure}</WeatherInfo__TableColumnDesc>
          </WeatherInfo__TableColumn>
        }
        {weather.temperature &&
          <WeatherInfo__TableColumn>
            <WeatherInfo__TableColumnTitle>
              <strong><Trans>Temperature</Trans>, K</strong>
            </WeatherInfo__TableColumnTitle>
            <WeatherInfo__TableColumnDesc>{weather.temperature}</WeatherInfo__TableColumnDesc>
          </WeatherInfo__TableColumn>
        }
        {weather.windSpeed &&
          <WeatherInfo__TableColumn>
            <WeatherInfo__TableColumnTitle>
              <strong><Trans>Wind</Trans>, m/s</strong>
            </WeatherInfo__TableColumnTitle>
            <WeatherInfo__TableColumnDesc>{weather.windSpeed}</WeatherInfo__TableColumnDesc>
          </WeatherInfo__TableColumn>
        }
      </WeatherInfo__Table>
    </WeatherInfo__Container>
  );
};
