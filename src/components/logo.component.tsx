import React from 'react';
import styled from 'styled-components';

const Logo = styled.img`
  width: 100%;
  max-width: 8rem;
  margin-left: 1rem;
  margin-right: 1rem;
`;

export const SiteLogo = () => {
  return (
    <Logo src="/assets/images/logo.png" alt="Logo"/>
  );
};
