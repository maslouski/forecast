import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';
import { MagnifyingGlass } from '@styled-icons/entypo/MagnifyingGlass';
import { Trans } from 'react-i18next';
import { setCityAsync } from '../redux/thunks';
import { selectStorageCities, selectCode, resetCode } from '../redux/slices/city.slice';

const Form__Wrapper = styled.div`
  text-align: center;
`;

const Form__Desctiption = styled.p`
  font-size: 1.4rem;
  line-height: 1.3em;
  margin-bottom: 2rem;
`;

const Form__Options = styled.ul`
  display: none;
  background-color: #fff;
  position: absolute;
  width: calc(100% - 2px);
  top: 100%;
  max-height: 200px;
  overflow-y: auto;
  border: 1px solid #ccc;
  border-top: none;
  border-bottom: none;

  &:hover {
    display: block;
  }
`;

const Form__Option = styled.button`
  width: 100%;
  margin: 0;
  padding: .8rem 2rem;
  font-size: 1.2rem;
  line-height: 1.3em;
  border-bottom: 1px solid #ccc;

  &:hover {
    background-color: rgba(0,0,0,0.1);
  }
`;

const Input = styled.input`
  width: 100%;
  padding: 1rem;
  padding-right: 4rem;
  font-size: 1.4rem;
  line-height: 1.3em;
  border: 1px solid #ccc;
  outline: none;
  box-shadow: none;
  border-radius: .2rem;

  &:focus + ul {
    display: block;
  }
`;

const Form = styled.form`
  position: relative;
  z-index: 100;
  width: 25rem;
  margin: 0 auto;
`;

const Submit = styled.button`
  width: 3rem;
  height: 3rem;
  margin: 0;
  padding: 0;
  position: absolute;
  top: 50%;
  right: .5rem;
  margin-top: -1.5rem;
  border-left: 1px solid #ccc;
  padding-left: .2rem;
  &[disabled] {
    cursor: not-allowed;
    opacity: .5;
  }
`;

export const SearchForm = () => {

  const dispatch = useDispatch();

  const storage = useSelector(selectStorageCities);
  const code = useSelector(selectCode);

  const [city, setCity] = useState('');

  const handleFormSubmit = (e: React.SyntheticEvent) => {
    e.preventDefault();
    dispatch(setCityAsync(city));
  };

  const handleCityChange = (val: string) => {
    setCity(val);
    if (code) {
      dispatch(resetCode());
    }
  };

  return (
    <Form__Wrapper>
      <Form__Desctiption><Trans>Want to check the weather</Trans></Form__Desctiption>
      <Form onSubmit={(e) => handleFormSubmit(e)}>
        <Submit
          disabled={!city}
          type="submit">
          <MagnifyingGlass/>
        </Submit>
        <Input value={city} onChange={(e) => handleCityChange(e.target.value)} type="text"/>
        <Form__Options>
          {storage
            .filter(item => item.toUpperCase().includes(city.toUpperCase()))
            .map(option => {
              return (
                <li key={`city-option-${option}`}>
                  <Form__Option
                    onClick={() => setCity(option)}
                    type="button">{option}
                  </Form__Option>
                </li>
              );
            }
            )}
        </Form__Options>
      </Form>
    </Form__Wrapper>
  );
};
