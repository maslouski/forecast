import { getEnvDomains } from '../shared';

const API_DOMAINS = {
  development: 'http://api.openweathermap.org',
  production: 'http://api.openweathermap.org',
};

export const { API_DOMAIN } = getEnvDomains(API_DOMAINS);
