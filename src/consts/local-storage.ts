export const LS_LOCALE = 'forecast_locale';
export const LS_CITIES = 'forecast_cities';
export const LS_THEME = 'forecast_theme';
