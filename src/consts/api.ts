import axios, { AxiosInstance } from 'axios';
import { API_DOMAIN } from './domains';
import { OPENWEATHERMAP_API_TOKEN } from './defaults';

interface Config extends AxiosInstance {
  openWeatherMapCalls?: {
    apiWeather: string,
  };
}

axios.defaults.headers.common = {};
axios.defaults.params = {};
axios.defaults.params.APPID = OPENWEATHERMAP_API_TOKEN;
const api: Config = axios.create();

// open weather map api
const apiDomain = `${API_DOMAIN}/data/2.5`;

// open weather map calls
api.openWeatherMapCalls = {
  apiWeather: `${apiDomain}/weather`,
};

export default api;
